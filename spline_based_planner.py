#!/usr/bin/env python
"""
Motion planner
"""

import numpy as np
import logging
from timeit import default_timer as timer
import matplotlib.pyplot as plt
from world import Vehicle
import itertools


class SplinePlanner(object):
    """
    Spline-based planner
    """

    def __init__(self, max_linear_velocity, max_angular_velocity, max_look_ahead_steps, neighborhood_radius):
        self.max_linear_velocity = max_linear_velocity
        self.max_angular_velocity = max_angular_velocity
        self.max_look_ahead_steps = max_look_ahead_steps
        self.neighborhood_radius = neighborhood_radius

    def plan(self, world, dt):
        """
        Planner
        """

        robot = world.vehicle
        # make a copy
        action_queue = list(robot.action_queue)
        robot_pose = np.array(robot.pose)

        # assume velocity profile is fixed, we only plan for steer angles
        # more advanced controller can consider different velocity profiles
        velocity_profile = self.max_linear_velocity

        # planning horizon
        planning_horizon = min(self.max_look_ahead_steps, robot.max_queue_size - len(action_queue))

        logging.info('**********************************new planning cycle***************************************')
        logging.info('Robot pose: {}, apply linear velocity profile: {}'.format(robot_pose, velocity_profile))

        # do not need to plan if there are enough actions for the robot
        if planning_horizon <= 0:
            logging.info('Robot action queue is full with {} actions'.format(len(action_queue)))
            return velocity_profile, None

        # predict robot pose
        predicted_pose = np.array(robot_pose)
        look_ahead_steps = 0
        for action in action_queue:
            predicted_pose = robot.dynamics(predicted_pose, action[0], action[1], dt)
            look_ahead_steps += 1

        # predict obstacle position
        obs_id_to_predicted_obs_pos_map = {}
        obs_id_to_obs_map = {}
        for obs in world.get_obstacles():
            # make a copy
            obs_pos = np.array(obs.position)
            for i in range(look_ahead_steps):
                obs_pos = obs.dynamics(obs_pos, obs.direction, obs.velocity, obs.world_size, dt)

            obs_id_to_predicted_obs_pos_map[obs.id] = obs_pos
            obs_id_to_obs_map[obs.id] = obs

        # start planning from predicted pose
        near_obs_ids = self._find_obstacles_in_neighborhood(obs_id_to_predicted_obs_pos_map, predicted_pose[:2],
                                                            self.neighborhood_radius)

        max_curvature = abs(float(self.max_angular_velocity) / velocity_profile)
        # For a curvature spline with degree d, we need d+1 control points, the search space size is set_size**(d+1)
        curvature_control_points_set_size = 5
        curvature_control_points_set = np.linspace(-max_curvature, max_curvature, curvature_control_points_set_size)

        # Use brute force search, more efficient approach could use memoization and store a look up table.
        # or we can segment the search space into smaller ones by some tricks and search in a smaller space
        # for example, if the set is [-2, -1, 0, 1, 2], we can look at the first control point, making it to be -2, 0, 2
        # respectively. The limits of the smaller search space defined by -2 is [-2, 2, 2] and [-2, -2, -2]. Similarly,
        # the limits of the smaller space defined by 0 is [0, 2, 2] and [0, -2, -2], and the one defined by 2 is
        # [2, 2, 2] and [2, -2, -2]. We look at the three smaller spaces, and choose the one which is most unlikely to
        # collide with  the obstacles by checking the nearest distance of their limits to the obstacles. We then search
        # inside the most unlikely collision space

        # spline degree
        spline_degree = 2
        curvature_control_points = itertools.product(curvature_control_points_set, repeat=spline_degree+1)

        safe_dist_to_wall = 1.0 / max_curvature
        safe_dist_to_obs = 0
        obstacles_in_world = world.get_obstacles()
        if obstacles_in_world:
            safe_dist_to_obs = obstacles_in_world[0].radius * 2

        logging.info('Look ahead steps(curr action queue size): {}, planning horizon: {}'.format(look_ahead_steps, planning_horizon))
        logging.info('Predicted robot pose: {}, number of obstacles in neighborhood: {}'.format(predicted_pose, len(near_obs_ids)))
        logging.info('Max curvature: {}, spline degree: {}, search size: {}'.format(max_curvature, spline_degree,
                                                                curvature_control_points_set_size**(spline_degree + 1)))
        logging.info('Safe dist to wall: {}, safe dist to obs: {}'.format(safe_dist_to_wall, safe_dist_to_obs))

        start_time = timer()
        planned_steers = []
        planned_steers_spline_control_points = []
        max_trajectory_reward = 0
        no_collision_set = []
        for points in curvature_control_points:
            trajectory = []
            steer_angles = self._get_trajectory_under_spline_based_planner(
                robot.dynamics,
                predicted_pose,
                velocity_profile,
                points,
                spline_degree,
                planning_horizon,
                dt,
                trajectory)

            trajectory_reward, time_steps_to_collide, no_collision_under_curr_control_points = self._calculate_trajectory_reward(
                world,
                near_obs_ids,
                obs_id_to_obs_map,
                obs_id_to_predicted_obs_pos_map,
                trajectory,
                dt,
                safe_dist_to_obs,
                safe_dist_to_wall)

            if no_collision_under_curr_control_points:
                no_collision_set.append(steer_angles[0])

            # here must be >, can not be >= to make sure we do find one which is feasible
            if trajectory_reward > max_trajectory_reward:
                max_trajectory_reward = trajectory_reward
                planned_steers = steer_angles[:time_steps_to_collide]
                planned_steers_spline_control_points = points

        logging.info('Chosen spline control points: {}'.format(planned_steers_spline_control_points))
        logging.info('Search for parameters takes {} seconds'.format(timer() - start_time))

        # if no planned_steers found
        if not planned_steers:
            logging.info('All candidate trajectories will collide with obstacles or end point too close to obstacles, '
                         'try to choose one from collision free trajectories whose end point may be close to obstacles')
            if not no_collision_set:
                logging.warn('Planned steers: empty, no trajectory is collision free')
            else:
                planned_steers = [no_collision_set[0]]
                logging.info('Planned steers: randomly chosen one from collision free traj without considering its end point dist to obstacles')

        logging.info('Planned steers: {}'.format(planned_steers))

        return velocity_profile, planned_steers

    def _find_obstacles_in_neighborhood(self, obs_id_to_predicted_obs_pos_map, vehicle_pos, neighborhood_radius):
        """
        Only consider obstacles near the vehicle
        """

        res = []
        for obs_id, predicted_pos in obs_id_to_predicted_obs_pos_map.items():
            if np.linalg.norm(predicted_pos - np.array(vehicle_pos)) <= neighborhood_radius:
                res.append(obs_id)

        return res

    def _get_trajectory_under_spline_based_planner(self,
                                                   robot_dynamics,
                                                   start_pose,
                                                   linear_velocity,
                                                   curvature_control_point,
                                                   spline_degree,
                                                   time_steps,
                                                   dt,
                                                   trajectory):
        """
        The control points are curvatures, use a B-spline to create a curve of curvatures
        """

        curvature_spline = Spline(curvature_control_point, spline_degree, dt * time_steps)
        times = np.linspace(0, dt * time_steps, time_steps + 1)
        steer_angles = [linear_velocity * curvature_spline.value(t) for t in times]

        # empty a list in place
        trajectory[:] = []
        trajectory.append(start_pose[:2])
        # make a copy
        next_pose = np.array(start_pose)
        for steer in steer_angles:
            next_pose = robot_dynamics(next_pose, linear_velocity, steer, dt)
            trajectory.append(next_pose[:2])

        return steer_angles

    def _calculate_trajectory_reward(
            self,
            world,
            near_obs_ids,
            obs_id_to_obs_map,
            obs_id_to_predicted_obs_pos_map,
            trajectory,
            dt,
            safe_dist_to_obs,
            safe_dist_to_wall):

        trajectory_reward = float('inf')
        time_steps_to_collide = 0
        no_collision_along_traj = True
        for obs_id in near_obs_ids:
            travel_time_to_collide_with_curr_obs, is_collision, cost_for_curr_obs = self._time_to_collision(
                world,
                obs_id_to_obs_map[obs_id],
                obs_id_to_predicted_obs_pos_map[obs_id],
                trajectory,
                dt,
                safe_dist_to_obs,
                safe_dist_to_wall)

            if cost_for_curr_obs < trajectory_reward:
                trajectory_reward = cost_for_curr_obs
                time_steps_to_collide = travel_time_to_collide_with_curr_obs

            if is_collision:
                no_collision_along_traj = False

        return trajectory_reward, time_steps_to_collide, no_collision_along_traj

    def _time_to_collision(
            self,
            world,
            obs,
            obs_start_position,
            trajectory,
            dt,
            safe_dist_to_obs,
            safe_dist_to_wall):
        """
        calculate time to collision along obstacles
        """

        travel_time_before_collision = 0
        cushion_steps = 6 # must >= 1
        is_collision = False

        obs_next_pos = np.array(obs_start_position)
        obs_trajectory = []
        for pos_index, robot_next_pos in enumerate(trajectory):
            obs_trajectory.append(obs_next_pos)

            if np.linalg.norm(obs_next_pos - robot_next_pos) < obs.radius or \
                    world.is_collision_to_wall(robot_next_pos):
                # max travel time is pos_index minus 2, because we want to leave some cushion, so next planing cycle, we
                # are able to escape
                travel_time_before_collision = max(travel_time_before_collision, pos_index - cushion_steps)
                is_collision = True
                break
            else:
                travel_time_before_collision = max(travel_time_before_collision, pos_index)
                obs_next_pos = obs.dynamics(obs_next_pos, obs.direction, obs.velocity, obs.world_size, dt)

        if not is_collision:
            # we do not collide with obs or walls if moving along the trajectory, and we can reach the end of the
            # trajectory, but we need to make sure we do not run two close to obstacles or walls in the end
            if np.linalg.norm(obs_next_pos - robot_next_pos) < safe_dist_to_obs or world.is_collision_to_wall(robot_next_pos, safe_dist_to_wall):
                travel_time_before_collision -= cushion_steps

        end_point_dist_to_obs = np.linalg.norm(trajectory[travel_time_before_collision] - obs_trajectory[travel_time_before_collision])
        end_point_dist_to_wall = world.dist_to_wall(trajectory[travel_time_before_collision])

        # weights on cost function
        weight_on_travel_time, weight_on_dist_to_obs = 0, 1
        cost = weight_on_travel_time * travel_time_before_collision + weight_on_dist_to_obs * min(end_point_dist_to_obs, end_point_dist_to_wall)

        return travel_time_before_collision, is_collision, cost


class Spline(object):
    """
    Spline
    """

    def __init__(self, control_points, degree, time_span=1):
        self.control_points = self._initialize_control_points(control_points)
        self.degree = degree
        # internal knots
        internal_knots = np.linspace(0, time_span, len(self.control_points) - self.degree + 1)
        # use clamped spline, so make the first d knots and last d knots equal
        # this way we will make sure the spline cross the last control point
        self.knots = np.append(np.ones(self.degree) * internal_knots[0], internal_knots)
        self.knots = np.append(self.knots, np.ones(self.degree) * internal_knots[-1])

    def value(self, t):
        res = 0
        for i, point in enumerate(self.control_points):
            res += self._b_i_d(i, self.degree, t) * point

        return res

    def _b_i_0(self, i, t):
        """
        Calculate the basis function B_{i,0}(t) = 1 if t_i <= t < t_{i+1} else 0
        :param i:
        :param t:
        :return:
        """

        if i > len(self.control_points) + self.degree - 1:
            raise ValueError('knots vector index out of range')

        # if two time knots are equal, return 0
        if self.knots[i] == self.knots[i+1]:
            return 0
        else:
            # here we make t <= self.knots[i+1] instead of <, to make sure the last point on the spline curve coincide
            # with the last control point. If we use < here, the last point will be 0 on the spline curve if t=1
            return 1 if self.knots[i] <= t <= self.knots[i+1] else 0

    def _b_i_d(self, i, d, t):
        """
        Calculate basis function for higher degrees B_{i,d}(t), here recursion is used, which is not good if d is large
        More efficient implementation should use matrix representation of B-splines
        :param i:
        :param d: degree
        :param t:
        :return:
        """

        if d == 0:
            return self._b_i_0(i, t)
        else:
            # deal with two time knots are equal
            tolerance = 1e-12
            if abs(self.knots[i + d] - self.knots[i]) < tolerance and abs(self.knots[i + d + 1] - self.knots[i + 1]) < tolerance:
                return 0
            elif abs(self.knots[i + d] - self.knots[i]) < tolerance:
                return (self.knots[i + 1 + d] - t) / (self.knots[i + d + 1] - self.knots[i + 1]) * self._b_i_d(i + 1, d-1, t)
            elif abs(self.knots[i + d + 1] - self.knots[i + 1]) < tolerance:
                return (t - self.knots[i]) / (self.knots[i + d] - self.knots[i]) * self._b_i_d(i, d-1, t)
            else:
                return (t - self.knots[i]) / (self.knots[i + d] - self.knots[i]) * self._b_i_d(i, d-1, t) + \
                       (self.knots[i + 1 + d] - t) / (self.knots[i + d + 1] - self.knots[i + 1]) * self._b_i_d(i + 1, d-1, t)

    def _initialize_control_points(self, control_points):
        res = []
        for point in control_points:
            res.append(np.array(point))

        return res


class LogUtil(object):
    """
    Log util
    """

    @staticmethod
    def set_up_logging(log_file_name, logging_level=logging.DEBUG):
        logging.basicConfig(filename=log_file_name, filemode='w', level=logging_level,
                            format='%(asctime)s %(message)s')
        # logging.getLogger().addHandler(logging.StreamHandler())

    @staticmethod
    def log_dict(one_dict, dict_info):
        logging.debug('--------------Printing {}-----------------'.format(dict_info))
        for k in one_dict:
            logging.debug('key: {}, value: {}'.format(k, one_dict[k]))


if __name__ == '__main__':
    # test Spline class
    control_points = [[0, 0], [1, 1], [2, 0], [3, 1], [4, 0]]
    spline = Spline(control_points, 2)
    times = np.linspace(0, 1, 50)
    values = np.array([spline.value(t) for t in times])

    plt.figure()
    plt.plot(values[:, 0], values[:, 1], 'ro')

    # test SplinePlanner class
    dt = 0.1
    max_linear_velocity, max_angular_velocity, max_look_ahead_steps, neighbor_hood = 0.8, np.pi / 3.0, 10, 5
    spline_degree = 2
    planner = SplinePlanner(max_linear_velocity, max_angular_velocity, max_look_ahead_steps, neighbor_hood)
    robot = Vehicle([1, 1, np.pi / 4.0])
    # curvature_control_points = [[2.5, 1.5, -12.5], [2.5, 0.5, -5], [2.5, 0.5, 0.5], [0.5, -0.5, 0], [0.5, -0.5, -0.5],
    #                             [0.5, -0.5, 0.5], [0, 0.5, -0.5], [0, 0.6, 0.8], [0, 0.8, 0.6], [0, -0.6, 0.8], [0, -0.8, 0.6]]
    max_curvature = 3
    curvature_control_points = itertools.product(np.linspace(- max_curvature, max_curvature, 5), repeat=spline_degree+1)

    plt.figure()
    for points in curvature_control_points:
        trace = []
        planned_steers = planner._get_trajectory_under_spline_based_planner(
                                                  robot.dynamics,
                                                  robot.pose,
                                                  max_linear_velocity,
                                                  points,
                                                  spline_degree,
                                                  max_look_ahead_steps,
                                                  dt,
                                                  trace)
        trace = np.array(trace)
        plt.plot(trace[:, 0], trace[:, 1], 'o-', label=str(points))

    plt.legend(loc=2)
    plt.show()

