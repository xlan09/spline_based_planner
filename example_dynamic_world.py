#!/usr/bin/env python

from world import World
import numpy as np
from time import sleep
from spline_based_planner import SplinePlanner, LogUtil

# setup logging
LogUtil.set_up_logging('SplineBasedPlanner.txt')

# init world
start_pose = np.array([1.0, 1.0, np.pi / 4.0])

world = World(
    start_pose=start_pose
)
# populate obstacle field with dynamic obstacles
world.generate_obstacles(n_obstacles=20, static=False)
# init pygame screen for visualization
screen = world.init_screen()
# get vehicle
vehicle = world.get_vehicle()

max_linear_velocity = 0.6
max_angular_velocity = np.pi / 3.0

# timestep for world update
dt = 0.1

# spline_based planner
max_look_ahead_steps = 12
neighborhood_radius = max(world.size[0], world.size[1]) / 3.0
planner = SplinePlanner(max_linear_velocity, max_angular_velocity, max_look_ahead_steps, neighborhood_radius)

elapsed_time_step = 0
planning_period = 3
allowed_planning_time_steps = 2
while True:
    # collision testing
    if world.in_collision():
        print 'Collision'
        break

    if np.mod(elapsed_time_step, int(planning_period)) == 0:
        # reset counter
        if elapsed_time_step == planning_period * 1000:
            elapsed_time_step = 0

        velocity_profile, planned_steers = planner.plan(world, dt)
        if planned_steers is not None:
            for steer in planned_steers:
                vehicle.add_to_action_queue(velocity_profile, steer)

        # assume planning takes some time, world is changing, forward the world
        # TO_DO: use a different thread to run the planner and move this
        for i in range(allowed_planning_time_steps):
            world.update(dt)
            # draw world
            world.draw(screen)
            elapsed_time_step += 1
    else:
        # update world
        world.update(dt)
        # draw world
        world.draw(screen)
        elapsed_time_step += 1

# provide some time to view result
sleep(10.0)
