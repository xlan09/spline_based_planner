# Goal
- Control a nonholonomic vehicle with fixed move-forward velocity to avoid dynamic obstacles.

- Perfect information about the environment: noise-free vehicle position and orientation, noise-free obstacle position and orientation, no model uncertainties for vehicle and obstacle dynamics, perfect prediction for vehicle and obstacle pose.

# Controller
- Spline-based controller(planner).

- At each planning cycle, forward predict vehicle and obstacle positions for a dynamic horizon, planning will start from the end of the look-ahead horizon. This will account for the time consumed for planning.

- When planning, search for all the obstacles inside a neighborhood of the vehicle, only consider the obstacles inside the neighborhood, use receding horizon to look ahead, maintain a dynamic window size,  search for all the candidate spline trajectories parametrized by 3 control points, choose the one which is collision free and farthest away from walls and obstacles.

- ![Gui does not show properly, click the png files to view](SplineBasedPlanner.png)

# GUI
- ![Gui does not show properly, click the png files to view](ObstacleAvoidanceGui.png)
