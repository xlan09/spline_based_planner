#!/usr/bin/env python

import numpy as np
import pygame
from copy import copy, deepcopy
import math
import logging


class Obstacle(object):

    def __init__(
        self,
        id,
        world_size,
        radius=0.2,
        color=(255.0, 0.0, 0.0),
        static=False
    ):
        self.id = id
        self.radius = radius
        self.color = color
        self.world_size = world_size
        self.static = static

        # select random direction and velocity
        self.position = np.random.random(2) * np.array(world_size)
        self.direction = np.random.random() * 2.0 * np.pi

        # initialize obstacle velocity
        self.velocity = 0.0
        if not self.static:
            self.velocity = np.random.random()

    def in_collision(self, point, radius=None):
        if radius is None:
            radius = self.radius

        distance = np.linalg.norm(point - self.position)
        if distance < radius:
            return True
        return False

    def update(self, dt):
        '''
        updates state of obstacle
        '''
        self.position = self.dynamics(self.position, self.direction, self.velocity, self.world_size, dt)

    def dynamics(self, curr_position, curr_direction, curr_velocity, world_size, dt):
        """
        obstacle dynamics
        """
        next_pos = np.array(curr_position) + np.array([np.cos(curr_direction), np.sin(curr_direction)]) * curr_velocity * dt

        return np.mod(next_pos, world_size)

    def draw(self, screen, px2m):
        curr_pos = (self.position * px2m).astype(int)
        pygame.draw.circle(
            screen,
            self.color,
            curr_pos,
            int(self.radius * px2m)
        )

        myfont = pygame.font.SysFont("monospace", 15)

        # render text
        label = myfont.render(str(self.id), 1, (255, 255, 0))
        screen.blit(label, curr_pos)


class Vehicle(object):
    """
    Differential Drive Vehicle Model

    _pose: [x, y, theta] in [meters, meters, radians]
    _linear_velocity: m/s
    _angular_velocity: rad/s
    """
    def __init__(
        self,
        initial_pose,
        initial_linear_velocity=0.0,
        initial_angular_velocity=0.0,
        color=(0, 255, 0)
    ):
        self.color = color
        self.pose = np.array(initial_pose)
        self.linear_velocity = initial_linear_velocity
        self.angular_velocity = initial_angular_velocity
        self.action_queue = []
        self.max_queue_size = 30
        self.trace = [[self.pose[0], self.pose[1]]]

    def add_to_action_queue(self, linear_velocity, angular_velocity):
        if len(self.action_queue) < self.max_queue_size:
            self.action_queue.append([linear_velocity, angular_velocity])
        else:
            logging.warning('robot action queue is full, will NOT add')

    def get_curr_commands(self):
        return self.linear_velocity, self.angular_velocity

    def get_state(self):
        '''
        Return pose where pose is [x, y, theta]
        '''

        return copy(self.pose)

    def get_velocity(self):
        """
        Get velocity
        """

        return np.array([np.cos(self.pose[2]), np.sin(self.pose[2])]) * self.linear_velocity

    def update(
        self,
        dt,
        eps=1e-12
    ):
        self.trace.append(self.pose[:2])

        # update commands
        if self.action_queue:
            self.linear_velocity, self.angular_velocity = self.action_queue[0]
            self.action_queue = self.action_queue[1:]
        else:
            # if no actions, set commands to be 0, so robot will stop
            self.linear_velocity = 0
            self.angular_velocity = 0

        self.pose = self.dynamics(self.pose, self.linear_velocity, self.angular_velocity, dt)

    def dynamics(self, curr_pose, linear_velocity, angular_velocity, dt):
        """
        robot dynamics
        """

        tolerance = 1e-12
        x, y, theta = curr_pose
        # update model
        if np.abs(angular_velocity) < tolerance:
            direction = theta + angular_velocity * dt
            x += linear_velocity * np.cos(direction) * dt
            y += linear_velocity * np.sin(direction) * dt
        else:
            old_theta = theta
            radius = linear_velocity / angular_velocity
            theta = theta + angular_velocity * dt
            x += radius * (np.sin(theta) - np.sin(old_theta))
            y -= radius * (np.cos(theta) - np.cos(old_theta))

        if theta > math.pi:
            theta -= 2 * math.pi
        elif theta < -1.0 * math.pi:
            theta += 2 * math.pi

        return np.array([x, y, theta])

    def draw(self, screen, px2m):
        # draw a triangle representing vehicle
        base_coords = np.array([
            [0.0, -0.05],
            [0.0, 0.05],
            [0.15, 0.0],
            [0.0, -0.05]
        ])

        theta = self.pose[2]
        rot = np.array([[np.cos(theta), -np.sin(theta)],
                        [np.sin(theta), np.cos(theta)]])
        coords = np.dot(base_coords, rot.T)
        pygame.draw.lines(
            screen,
            self.color,
            True,
            np.int32((coords + self.pose[:2]) * px2m)
        )

        # draw trace
        pygame.draw.lines(
            screen,
            [255, 255, 255],
            False,
            np.int32(np.array(self.trace) * px2m)
        )


class World(object):

    def __init__(
        self,
        world_size=np.array([8.0, 8.0]),
        start_pose=np.array([1.0, 1.0, 0.0]),
        px2m=100
    ):
        self.size = world_size
        self.px2m = px2m

        self.start_pose = start_pose
        self.vehicle = Vehicle(start_pose)
        self.obstacles = []

    def generate_obstacles(
        self,
        n_obstacles=10,
        obstacle_radius=0.2,
        static=False
    ):
        # randomly generate obstacle field
        self.obstacles = []
        for _ in xrange(0, n_obstacles):
            # resample obstacle until it isn't in collision
            while True:
                obstacle = Obstacle(
                    len(self.obstacles) + 1,
                    self.size,
                    radius=obstacle_radius,
                    static=static
                )
                if not obstacle.in_collision(
                        self.start_pose[:2],
                    radius=5.0 * obstacle_radius
                ):
                    break

            self.obstacles.append(obstacle)

    def set_obstacles(self, obstacles):
        # manually set obstacle field
        self.obstacles = obstacles

    def init_screen(self):
        pygame.init()
        screen = pygame.display.set_mode(
            np.int32(self.size * self.px2m)
        )
        pygame.display.set_caption('obstacle field')

        screen.fill((0, 0, 30))
        pygame.display.flip()
        return screen

    def get_obstacles(self):
        return self.obstacles

    def get_vehicle(self):
        return self.vehicle

    def get_size(self):
        return self.size

    def in_collision(self):
        vehicle_pos = self.vehicle.get_state()[:2]
        is_collision = self.is_collision_to_wall(vehicle_pos, 0)

        if is_collision:
            return True
        else:
            # check obstacle collisions
            for obstacle in self.obstacles:
                if obstacle.in_collision(vehicle_pos):
                    return True

            return False

    def is_collision_to_wall(self, vehicle_pos, safe_dist_to_wall=0):
        # check world bounds
        if vehicle_pos[0] < safe_dist_to_wall \
                or vehicle_pos[1] < safe_dist_to_wall \
                or vehicle_pos[0] > self.size[0] - safe_dist_to_wall \
                or vehicle_pos[1] > self.size[1] - safe_dist_to_wall:
            return True
        else:
            return False

    def dist_to_wall(self, position):
        vec_to_NECorner = np.array(self.size) - np.array(position)
        return min(np.min(np.abs(position)), np.min(np.abs(vec_to_NECorner)))

    def update(self, dt):
        self.vehicle.update(dt)
        for obstacle in self.obstacles:
            obstacle.update(dt)

    def draw(self, screen):
        screen.fill((0, 0, 30))
        self.vehicle.draw(screen, self.px2m)
        for obstacle in self.obstacles:
            obstacle.draw(screen, self.px2m)
        pygame.display.flip()

    def get_snapshot(self):
        return deepcopy(self)


if __name__ == "__main__":
    world = World()
    screen = world.init_screen()
